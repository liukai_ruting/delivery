package com.rutingtech.delivery.api.service;

import com.rutingtech.delivery.api.domain.dao.WarehouseDao;
import com.rutingtech.delivery.api.domain.dto.MapListDto;
import com.rutingtech.delivery.api.domain.entity.Warehouse;
import com.rutingtech.delivery.api.domain.mapper.WarehouseMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liukai
 * @date 2017/8/30
 */

@Service
public class WarehouseService implements BaseService<Warehouse> {

    @Resource
    private WarehouseDao warehouseDao;

    @Resource
    private WarehouseMapper warehouseMapper;

    public List<Warehouse> findAll() {
        return warehouseDao.findAll();
    }

    public long count() {
        return warehouseDao.count();
    }

    public List<MapListDto> findAll(int pageNum, int pageSize) {
        long offset = (pageNum - 1) * pageSize;
        return warehouseMapper.findByPage(offset, pageSize);
    }
}