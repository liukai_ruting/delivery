package com.rutingtech.delivery.api.domain.mapper;

import com.rutingtech.delivery.api.domain.dto.MapListDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author liukai
 * @date 2017/9/4
 */

@Mapper
public interface CompanyMapper extends BaseMapper<MapListDto> {

    List<String> groupByRegion();
}