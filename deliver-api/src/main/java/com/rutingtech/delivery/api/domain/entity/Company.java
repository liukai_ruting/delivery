package com.rutingtech.delivery.api.domain.entity;

import javax.persistence.*;

/**
 * @author liukai
 * @date 2017/8/30
 */

@Entity
@Table(name = "company", schema = "scilog", catalog = "")
public class Company {
    private long id;
    private String lat;
    private String lng;
    private long stationRegionId;
    private long warehouseId;
    private boolean status;
    private long cityId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "lat", nullable = false, length = 20)
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @Basic
    @Column(name = "lng", nullable = false, length = 20)
    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @Basic
    @Column(name = "station_region_id", nullable = false)
    public long getStationRegionId() {
        return stationRegionId;
    }

    public void setStationRegionId(long stationRegionId) {
        this.stationRegionId = stationRegionId;
    }

    @Basic
    @Column(name = "warehouse_id", nullable = false)
    public long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(long warehouseId) {
        this.warehouseId = warehouseId;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Basic
    @Column(name = "city_id", nullable = false)
    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Company that = (Company) o;

        if (id != that.id) return false;
        if (stationRegionId != that.stationRegionId) return false;
        if (warehouseId != that.warehouseId) return false;
        if (status != that.status) return false;
        if (cityId != that.cityId) return false;
        if (lat != null ? !lat.equals(that.lat) : that.lat != null) return false;
        if (lng != null ? !lng.equals(that.lng) : that.lng != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (lat != null ? lat.hashCode() : 0);
        result = 31 * result + (lng != null ? lng.hashCode() : 0);
        result = 31 * result + (int) (stationRegionId ^ (stationRegionId >>> 32));
        result = 31 * result + (int) (warehouseId ^ (warehouseId >>> 32));
        result = 31 * result + (status ? 1 : 0);
        result = 31 * result + (int) (cityId ^ (cityId >>> 32));
        return result;
    }
}
