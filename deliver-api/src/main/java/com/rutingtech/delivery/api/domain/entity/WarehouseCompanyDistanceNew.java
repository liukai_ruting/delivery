package com.rutingtech.delivery.api.domain.entity;

import javax.persistence.*;

/**
 * @author liukai
 * @date 2017/8/30
 */
@Entity
@Table(name = "warehouse_company_distance_new", schema = "scilog", catalog = "")
public class WarehouseCompanyDistanceNew {
    private long id;
    private long companyId;
    private long warehouseId;
    private double distance;
    private String duration;
    private long stationRegionId;
    private long cityId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "company_id", nullable = false)
    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "warehouse_id", nullable = false)
    public long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(long warehouseId) {
        this.warehouseId = warehouseId;
    }

    @Basic
    @Column(name = "distance", nullable = false, precision = 2)
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Basic
    @Column(name = "duration", nullable = false, length = 50)
    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "station_region_id", nullable = false)
    public long getStationRegionId() {
        return stationRegionId;
    }

    public void setStationRegionId(long stationRegionId) {
        this.stationRegionId = stationRegionId;
    }

    @Basic
    @Column(name = "city_id", nullable = false)
    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WarehouseCompanyDistanceNew that = (WarehouseCompanyDistanceNew) o;

        if (id != that.id) return false;
        if (companyId != that.companyId) return false;
        if (warehouseId != that.warehouseId) return false;
        if (Double.compare(that.distance, distance) != 0) return false;
        if (stationRegionId != that.stationRegionId) return false;
        if (cityId != that.cityId) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (companyId ^ (companyId >>> 32));
        result = 31 * result + (int) (warehouseId ^ (warehouseId >>> 32));
        temp = Double.doubleToLongBits(distance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (int) (stationRegionId ^ (stationRegionId >>> 32));
        result = 31 * result + (int) (cityId ^ (cityId >>> 32));
        return result;
    }
}
