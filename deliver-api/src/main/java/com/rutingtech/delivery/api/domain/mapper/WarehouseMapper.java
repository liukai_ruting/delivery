package com.rutingtech.delivery.api.domain.mapper;

import com.rutingtech.delivery.api.domain.dto.MapListDto;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liukai
 * @date 2017/9/4
 */

@Mapper
public interface WarehouseMapper extends BaseMapper<MapListDto> {

}
