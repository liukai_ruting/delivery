package com.rutingtech.delivery.api.controller;

import com.rutingtech.delivery.api.service.CompanyService;
import com.rutingtech.delivery.api.service.WarehouseService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liukai
 * @date 2017/8/29
 */

@RequestMapping("/map")
@RestController
public class MapController {

    @Resource
    private CompanyService companyService;

    @Resource
    private WarehouseService warehouseService;

    @GetMapping("/index")
    public ModelAndView index() {
        return new ModelAndView("/index");
    }

    @GetMapping("/data")
    public Map<String, List> getData() {
        List companys = companyService.findAll();
        List warehouses = warehouseService.findAll();

        Map<String, List> res = new HashMap<>();
        res.put("companys", companys);
        res.put("warehouse", warehouses);

        return res;
    }
}
