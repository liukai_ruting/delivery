package com.rutingtech.delivery.api.domain.dao;

import com.rutingtech.delivery.api.domain.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author liukai
 * @date 2017/8/28
 */

@Repository
public interface CompanyDao extends JpaRepository<Company, Long> {

}
