package com.rutingtech.delivery.api.domain.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liukai
 * @date 2017/9/4
 */

public interface BaseMapper<T> {

    Long findAllNum();

    List<T> findByPage(@Param("offset") long offset,
                                @Param("pageSize") int pageSize);
}
