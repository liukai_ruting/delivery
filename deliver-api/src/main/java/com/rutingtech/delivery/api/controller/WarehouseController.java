package com.rutingtech.delivery.api.controller;

import com.rutingtech.delivery.api.common.PageResultBean;
import com.rutingtech.delivery.api.common.ResultBean;
import com.rutingtech.delivery.api.domain.dto.MapListDto;
import com.rutingtech.delivery.api.domain.entity.Warehouse;
import com.rutingtech.delivery.api.service.WarehouseService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liukai
 * @date 2017/9/1
 */

@RequestMapping("/warehouse")
@RestController
public class WarehouseController {

    @Resource
    private WarehouseService warehouseService;

    @GetMapping("/all")
    public ResultBean<List<Warehouse>> findAll() {
        return new ResultBean<>(warehouseService.findAll());
    }

    @GetMapping("/page/{pageNum}/{pageSize}")
    public PageResultBean<List<MapListDto>> getByPage(@PathVariable Integer pageNum,
                                                      @PathVariable Integer pageSize) {
        return new PageResultBean<>(
                warehouseService.findAll(pageNum, pageSize),
                warehouseService.count(),
                pageNum,
                pageSize);
    }
}