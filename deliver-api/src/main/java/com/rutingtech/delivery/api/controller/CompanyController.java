package com.rutingtech.delivery.api.controller;

import com.rutingtech.delivery.api.common.PageResultBean;
import com.rutingtech.delivery.api.common.ResultBean;
import com.rutingtech.delivery.api.domain.dto.MapListDto;
import com.rutingtech.delivery.api.domain.entity.Company;
import com.rutingtech.delivery.api.service.CompanyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liukai
 * @date 2017/9/1
 */

@RequestMapping("/company")
@RestController
public class CompanyController {

    @Resource
    private CompanyService companyService;

    @GetMapping("/all")
    public ResultBean<List<Company>> findAll() {
        return new ResultBean<>(companyService.findAll());
    }

    @GetMapping("/page/{pageNum}/{pageSize}")
    public PageResultBean<List<MapListDto>> findAll(@PathVariable Integer pageNum,
                                                      @PathVariable Integer pageSize) {
        return new PageResultBean<>(
                companyService.findAll(pageNum, pageSize),
                companyService.count(),
                pageNum,
                pageSize);
    }

    @GetMapping("/group")
    public ResultBean<List<String>> groupByRegion() {
        return new ResultBean<>(companyService.groupByRegion());
    }
}