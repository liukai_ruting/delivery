package com.rutingtech.delivery.api.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liukai
 * @date 2017/9/1
 */

@Data
public class PageResultBean<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int SUCCESS = 0;
    public static final int FAIL = 1;
    public static final int NO_PERMISSION = 2;

    private String msg = "success";

    private int code = SUCCESS;

    private T data;

    private long total;

    private int pageNum;

    private int pageSize;

    public PageResultBean() {
        super();
    }

    public PageResultBean(T data, long total, int pageNum, int pageSize) {
        super();
        this.data = data;
        this.total = total;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public PageResultBean(Throwable e) {
        super();
        this.msg = e.toString();
        this.code = FAIL ;
    }
}
