package com.rutingtech.delivery.api.service;

import com.rutingtech.delivery.api.domain.dto.MapListDto;

import java.util.List;

/**
 * @author liukai
 * @date 2017/9/4
 */

public interface BaseService<T> {

    List<T> findAll();

    long count();

    List<MapListDto> findAll(int pageNum, int pageSize);
}
