package com.rutingtech.delivery.api.domain.entity;

import javax.persistence.*;

/**
 * @author liukai
 * @date 2017/8/30
 */
@Entity
@Table(name = "company_distance", schema = "scilog", catalog = "")
public class CompanyDistance {
    private long id;
    private long originCompanyId;
    private long targetCompanyId;
    private double distance;
    private String duration;
    private long stationRegionId;
    private long cityId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "origin_company_id", nullable = false)
    public long getOriginCompanyId() {
        return originCompanyId;
    }

    public void setOriginCompanyId(long originCompanyId) {
        this.originCompanyId = originCompanyId;
    }

    @Basic
    @Column(name = "target_company_id", nullable = false)
    public long getTargetCompanyId() {
        return targetCompanyId;
    }

    public void setTargetCompanyId(long targetCompanyId) {
        this.targetCompanyId = targetCompanyId;
    }

    @Basic
    @Column(name = "distance", nullable = false, precision = 2)
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Basic
    @Column(name = "duration", nullable = false, length = 50)
    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "station_region_id", nullable = false)
    public long getStationRegionId() {
        return stationRegionId;
    }

    public void setStationRegionId(long stationRegionId) {
        this.stationRegionId = stationRegionId;
    }

    @Basic
    @Column(name = "city_id", nullable = false)
    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CompanyDistance that = (CompanyDistance) o;

        if (id != that.id) return false;
        if (originCompanyId != that.originCompanyId) return false;
        if (targetCompanyId != that.targetCompanyId) return false;
        if (Double.compare(that.distance, distance) != 0) return false;
        if (stationRegionId != that.stationRegionId) return false;
        if (cityId != that.cityId) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (originCompanyId ^ (originCompanyId >>> 32));
        result = 31 * result + (int) (targetCompanyId ^ (targetCompanyId >>> 32));
        temp = Double.doubleToLongBits(distance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (int) (stationRegionId ^ (stationRegionId >>> 32));
        result = 31 * result + (int) (cityId ^ (cityId >>> 32));
        return result;
    }
}
