package com.rutingtech.delivery.api.service;

import com.rutingtech.delivery.api.domain.dao.CompanyDao;
import com.rutingtech.delivery.api.domain.dto.MapListDto;
import com.rutingtech.delivery.api.domain.entity.Company;
import com.rutingtech.delivery.api.domain.mapper.CompanyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liukai
 * @date 2017/8/30
 */

@Service
public class CompanyService implements BaseService<Company> {

    @Resource
    private CompanyDao companyDao;

    @Resource
    private CompanyMapper companyMapper;

    public List<Company> findAll() {
        return companyDao.findAll();
    }

    public long count() {
        return companyDao.count();
    }

    public List<MapListDto> findAll(int pageNum, int pageSize) {
        long offset = (pageNum -1 ) * pageSize;
        return companyMapper.findByPage(offset, pageSize);
    }

    public List<String> groupByRegion() {
        return companyMapper.groupByRegion();
    }
}