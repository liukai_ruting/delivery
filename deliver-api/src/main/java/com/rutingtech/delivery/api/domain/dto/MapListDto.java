package com.rutingtech.delivery.api.domain.dto;

import lombok.Data;

/**
 * @author liukai
 * @date 2017/9/4
 */

@Data
public class MapListDto {

    private long id;

    private String lng;

    private String lat;
}
