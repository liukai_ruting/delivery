package com.rutingtech.delivery.api.domain.mapper;

import com.rutingtech.delivery.api.Application;
import com.rutingtech.delivery.api.domain.dto.MapListDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liukai
 * @date 2017/9/4
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class WarehouseMapperTest {

    @Resource
    private WarehouseMapper warehouseMapper;

    @Test
    public void findAllNumTest() throws Exception {
        System.out.println(warehouseMapper.findAllNum());
    }

    @Test
    public void findAllPageTest() {
        List<MapListDto> warehouse1 =  warehouseMapper.findByPage(0, 10);
        System.out.println(warehouse1.size());
        List<MapListDto> warehouse2 = warehouseMapper.findByPage(1, 9);
        System.out.println(warehouse2.size());
    }
}