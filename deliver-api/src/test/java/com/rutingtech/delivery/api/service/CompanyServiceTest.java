package com.rutingtech.delivery.api.service;

import com.rutingtech.delivery.api.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liukai
 * @date 2017/9/1
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class CompanyServiceTest {

    @Resource
    private CompanyService companyService;

    @Test
    public void countTest() {
        System.out.println(companyService.count());
    }

    @Test
    public void findAllTest() throws Exception {
        List company1 = companyService.findAll(1, 20);
        System.out.println(company1);

        List company3 = companyService.findAll(2, 8);
        System.out.println(company3);
    }

    @Test
    public void groupByRegion() throws Exception {
        List<String> regions = companyService.groupByRegion();
        System.out.println(regions.size());
    }
}