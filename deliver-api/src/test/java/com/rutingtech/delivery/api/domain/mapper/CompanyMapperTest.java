package com.rutingtech.delivery.api.domain.mapper;

import com.rutingtech.delivery.api.Application;
import com.rutingtech.delivery.api.domain.dto.MapListDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liukai
 * @date 2017/9/4
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class CompanyMapperTest {

    @Resource
    private CompanyMapper companyMapper;

    @Test
    public void findAllNumTest() throws Exception {
        System.out.println(companyMapper.findAllNum());
    }

    @Test
    public void findAllPageTest() {
        List<MapListDto> companys1 =  companyMapper.findByPage(0, 10);
        System.out.println(companys1.size());
        List<MapListDto> companys2 = companyMapper.findByPage(1, 9);
        System.out.println(companys2.size());
    }

}