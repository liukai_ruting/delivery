package com.rutingtech.delivery.api.service;

import com.rutingtech.delivery.api.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liukai
 * @date 2017/9/1
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class WarehouseServiceTest {

    @Resource
    private WarehouseService warehouseService;

    @Test
    public void countTest() {
        System.out.println(warehouseService.count());
    }

    @Test
    public void findAll1Test() throws Exception {
        List warehouse = warehouseService.findAll(1, 10);
        System.out.println(warehouse.size());
    }
}