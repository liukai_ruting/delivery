## debug

1. Ajax 跨域

  - 参考：[Spring Boot设置跨域访问](http://www.jianshu.com/p/55643abe7a18)

  - 解决：添加配置
  ```java
  @Configuration
  public class CORSConfiguration {
      @Bean
      public WebMvcConfigurer corsConfigurer() {
          return new WebMvcConfigurerAdapter() {
              @Override
              public void addCorsMappings(CorsRegistry registry) {
                  registry.addMapping("/**")
                          .allowedHeaders("*")
                          .allowedMethods("*")
                          .allowedOrigins("*");
              }
          };
      }
  }
  ```
