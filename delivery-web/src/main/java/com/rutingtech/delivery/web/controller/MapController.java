package com.rutingtech.delivery.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author liukai
 * @date 2017/8/29
 */

@RequestMapping("/map")
@Controller
public class MapController {

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    @GetMapping("/all")
    public ModelAndView showAll() {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("map");
        return modelAndView;
    }
}
