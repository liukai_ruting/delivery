$(document).ready(function() {

    // 初始数据请求
    var response = $.ajax({
        url: "http://127.0.0.1:8080/company/page/1/10",
        async: false
    });

    $.ajax({
        url: "http://127.0.0.1:8080/company/group"
    });

    // 调用百度地图
    var map = new BMap.Map("allmap");
    map.centerAndZoom(new BMap.Point(116.331398, 39.897445), 11);
    map.enableScrollWheelZoom(true);

    // 初始地图
    initMap(map, response);

    // 初始列表
    initItems(response);

    // 形成分页
    initpagination(map, response);
});

function initMap(map, response) {
    var data = response.responseJSON.data;

    map.clearOverlays();

    for (var i = 0; i < data.length; ++i) {
        var lng = data[i].lng;
        var lat = data[i].lat;
        var new_point = new BMap.Point(lng, lat);
        addMarker(map, new_point);
    }
}

function initItems(response) {
    var data = response.responseJSON.data;
    console.log(data);

    var tem = doT.template($("#sites").html());
    $("#items").html(tem(data));
}

function initpagination(map, response) {
    var currentPage = response.responseJSON.page;
    var totalPage = response.responseJSON.total / response.responseJSON.pageSize;

    var options = {
        bootstrapMajorVersion: 3,
        size: "small",
        alignment: "center",
        shouldShowPage: true,
        currentPage: currentPage,
        totalPages: totalPage,
        numberOfPages: 4,
        itemTexts: function (type, page, current) {
            switch (type) {
                case "first":
                    return "首页";
                case "prev":
                    return "上一页";
                case "next":
                    return "下一页";
                case "last":
                    return "末页";
                case "page":
                    return page;
            }
        },
        onPageClicked: function (event, originalEvent, type, page) {
            var res = $.ajax({
                url: "http://127.0.0.1:8080/company/page/"+ page + "/10",
                async: false
            });
            initItems(res);
            initMap(map, res);
        }
    };
    $("#pagintor").bootstrapPaginator(options);
}

// 编写自定义函数,创建标注
function addMarker(map, point){
    var marker = new BMap.Marker(point);
    map.addOverlay(marker);
}

// 用经纬度设置地图中心点
function theLocation(response) {
    if ($("#longitude").val !== "" && $("#latitude").val !== "") {
        map.clearOverlays();

        for (var i = 0; i < response.companys.length; ++i) {
            var lng = response.companys[i].lng;
            var lat = response.companys[i].lat;
            var new_point = new BMap.Point(lng, lat);
            var marker = new BMap.Marker(new_point);
            map.addOverlay(marker);
            map.panTo(new_point);
        }
    }
}