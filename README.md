# delivery

### 项目构建

- 过程：由 IDEA 创建空项目

- 开发环境
    - IDE：IntelliJ IDEA 2017.2
    - OS：Windows 10/64bit
    
### 模块(Module)

- api

    - 描述：相关接口开发
    
    - 过程：由 IDEA 创建 Maven 模块
        
    - 开发环境：
        - 框架：Spring Boot 1.5.6.RELEASE
        - 构建工具：Maven 3.3.9

- web
    - 描述：相关页面开发
        
    - 过程：由 IDEA 创建 Maven 模块
    
    - 开发环境：
        - 框架：Spring Boot 1.5.6.RELEASE
        - 模板引擎：Thymeleaf
        - 前端样式框架：Bootstrap 3.3.7，Amaze UI 2.7.2
        - 构建工具：Maven 3.3.9